# HIP Vector Add

A simple "vector add" on HIP (CUDA on Radeon)

## License

This project is multi-licensed.
You can select the license of your choice from as following:

* [Creative Commons Attribution 4.0 International (CC-BY 4.0)](http://creativecommons.org/licenses/by/4.0/)
  * author (attribution) : Fixstars http://github.com/fixstars
* [The MIT License](https://opensource.org/licenses/mit-license.php)
  * year: 2016
  * copyright holders: Fixstars

## Desclaimer

Anyone including Fixstars is not responsible for any damages or corrupts by this project.
Download and use this project at your own risk.

Any questions or pull-resquests or other contribution are welcome and we're trying to respond as soon as possible.
But no one is responsible to respond them.
Any responses are not responsible by anyone including Fixstars.
