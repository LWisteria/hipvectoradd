#include <stdio.h>
#include <iostream>

#include "kernel.hpp"

__global__
void kernel(double z[], const double x[], const double y[])
{
	const auto i = (blockIdx.x * blockDim.x + threadIdx.x);

	z[i] = x[i] + y[i];
}


void Kernel(double z[], const double x[], const double y[],
        const std::size_t block, const std::size_t thread)
{
	kernel<<<block, thread>>>(z, x, y);
}
